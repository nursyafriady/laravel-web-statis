@extends ('layouts.default')

@section ('title')
    Register
@endsection

@section('content')
<h2>Buat Account Baru</h2>
    <h3>Sign Up form</h3>

    <form action="{{ route('welcome') }}" method="POST">
        @csrf
        <label for="firstname">First name:</label><br><br>
        <input type="text" id="firstname" name="firstname" value=""><br><br>
        <label for="lname">Last name:</label><br><br>
        <input type="text" id="lname" name="lname" value=""><br><br>        

        <label>Gender:</label><br><br>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label>
        <br><br>

        <label for="nationality">Nationality:</label>
        <br><br>
        <select name="nationality" id="nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="Singapura">Singapura</option>
            <option value="United Stated">United Stated</option>
            <option value="Other">Other</option>
        </select>
        <br><br>

        <label>Language Spoken:</label>
        <br><br>
        <input type="checkbox" id="indonesia" name="indonesia" value="Indonesia">
        <label for="indonesia"> Bahasa Indonesia </label><br>
        <input type="checkbox" id="english" name="english" value="English">
        <label for="english"> English </label><br>
        <input type="checkbox" id="other" name="other" value="Other">
        <label for="other"> Other </label>
        <br><br>

        <label for="pesan">Bio:</label>
        <br><br>        
        <textarea name="pesan" id="pesan" cols="40" rows="10"></textarea>
        <br>

        <input type="submit" value="Sign Up">
        <br><br><br>
    </form> 
@endsection