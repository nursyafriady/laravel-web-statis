<!DOCTYPE html>
<html lang="en">
<head>
    @include ('includes.meta')
</head>
<body>

    @yield('content') 
     
</body>
</html>
