<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view ('pages.register');
    }

    // public function welcome() {
    //     return view ('pages.welcome');
    // }

    public function welcome(Request $request) {
        $firstName = $request["firstname"];
        $lastName = $request["lname"];
        return view ('pages.welcome')
            ->with(['firstName' => $firstName, 'lastName' => $lastName ]);
    }
}
